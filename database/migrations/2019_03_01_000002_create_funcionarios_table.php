<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncionariosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'funcionarios';

    /**
     * Run the migrations.
     * @table funcionarios
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('cpf', 20)->primary();
            $table->unsignedInteger('empresa_id');
            $table->string('nome', 200);
            $table->string('telefone', 20);
            $table->string('email', 100);

            $table->unique(["email"], 'uk_email');

            $table->unique(["cpf"], 'cpf_empresa_id_UNIQUE');
            $table->index(["empresa_id"], 'funcionarios_empresa_id_fkey');
            $table->softDeletes();
            $table->nullableTimestamps();

            $table->foreign('empresa_id', 'funcionarios_empresa_id_fkey')
                ->references('id')->on('empresas')
                ->onDelete('restrict')
                ->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
