<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .form-area
        {
            background-color: #FAFAFA;
            padding: 10px 40px 60px;
            margin: 10px 0px 60px;
            border: 1px solid GREY;
        }
        .logo {
            display: inline-block;
            vertical-align: middle;
            height: 40px;
            margin: 5px 30px 0 0;

        }
    </style>
    <link href="{{asset('admin/css/menu.css')}}" rel="stylesheet" id="menu-css">
    <script>
        $(document).ready(function(){
            $('.top-line').after('<div class="mobile-menu d-xl-none">');
            $('.top-menu').clone().appendTo('.mobile-menu');
            $('.mobile-menu-btn').click(function(){
                $('.mobile-menu').stop().slideToggle();
            });
        });
    </script>
</head>
<body>
<div id="app">
    <header class="top-line">
        <img class="logo" src="http://controlaltweb.com.br/site/img/logo2-bco-header.png" alt="logo alt">
        <div class="phone"><i class="fa fa-phone"></i>(16) 3443-2244 </div>
        <div class="mobile-menu-btn"><i class="fa fa-bars"></i> Меню</div>
        <nav class="main-menu top-menu">
            <ul>
                @guest
                    <li>
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li>
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="active"><a href="/home">Home</a></li>
                    <li><a href="{{route('admin.empresas.index')}}">Empresas</a></li>
                    <li><a href="{{route('admin.funcionarios.index')}}">Funcionários</a></li>
                    <li class="nav-item dropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endguest
            </ul>
        </nav>
    </header>
    <div class="container">
        <br style="clear:both">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @elseif($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
    </div>

    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>
</html>
