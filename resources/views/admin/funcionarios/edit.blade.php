@extends('layouts.app')

@section('content')
    @include('admin.request-error')
    <div class="container">
        <div class="col-md-12">
            <div class="form-area">
                <form role="form" action="{{ route('admin.funcionarios.update', $funcionario->getKey()) }}" method="POST">
                    @method('PATCH')
                    @csrf
                    <br style="clear:both">
                    <h3 style="margin-bottom: 25px; text-align: center;">Editar Funcionário</h3>
                    <div class="form-group">
                        <label for="empresa_id">Empresa:</label><br>
                        <select class="form-control"  id="empresa_id" name="empresa_id">
                            <option value="">Selecione</option>
                            @foreach($empresas as $empresa)
                                <option value="{{$empresa->id}}" {{ $funcionario->empresa_id == $empresa->id?'selected':'' }}>{{$empresa->nome}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="cpf">CPF:</label><br>
                        <input type="text" class="form-control"  id="cpf" name="cpf" placeholder="CPF"  value="{{ $funcionario->cpf }}">
                    </div>
                    <div class="form-group">
                        <label for="Nome">Nome:</label><br>
                        <input type="text" class="form-control"  id="nome" name="nome" placeholder="Nome" value="{{ $funcionario->nome }}">
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail:</label><br>
                        <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="{{  $funcionario->email }}">
                    </div>
                    <div class="form-group">
                        <label for="url">Telefone:</label><br>
                        <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Web Site" value="{{  $funcionario->telefone }}">
                    </div>
                    <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Enviar</button>
                    <button type="button" id="voltar" name="voltar" onclick="window.location.href='{{ route('admin.funcionarios.index') }}'" class="btn btn-info pull-left">Voltar</button>
                </form>
            </div>
        </div>
    </div>
@endsection
