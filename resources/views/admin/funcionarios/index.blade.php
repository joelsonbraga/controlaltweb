@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <br style="clear:both">
            <div class="col-lg-12">
                <div class="float-lg-left">
                    <h2>Listar Funcionários</h2>
                </div>
                <div class="float-lg-right">
                    <a class="btn btn-success" href="{{ route('admin.funcionarios.create') }}"> Inserir</a>
                </div>
            </div>
        </div>
        <div class="row">
            <table class="table table-striped table-condensed">
                <thead>
                <tr>
                    <th>CPF</th>
                    <th>Nome</th>
                    <th>E-mails</th>
                    <th>Telefone</th>
                    <th>Empresa</th>
                    <th width="280px">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($funcionarios as $funcionario)
                    <tr>
                        <td>{{ $funcionario->cpf }}</td>
                        <td>{{ $funcionario->nome }}</td>
                        <td>{{ $funcionario->email }}</td>
                        <td>{{ $funcionario->telefone }}</td>
                        <td>{{ $funcionario->empresas->nome }}</td>
                        <td>
                            <form action="{{ route('admin.funcionarios.destroy', $funcionario->cpf) }}" method="POST">
                                <a class="btn btn-primary" href="{{ route('admin.funcionarios.edit', (string) $funcionario->getKey()) }}">Editar</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger" onclick="javascript: return confirm('Deseja realamente exluir?');">Excluir</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $funcionarios->links() !!}
        </div>
    </div>
@endsection
