@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <br style="clear:both">
            <div class="col-lg-12">
                <div class="float-lg-left">
                    <h2>Listar Empresas</h2>
                </div>
                <div class="float-lg-right">
                    <a class="btn btn-success" href="{{ route('admin.empresas.create') }}"> Inserir</a>
                </div>
            </div>
        </div>
        <div class="row">
            <table class="table table-striped table-condensed">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>E-mails</th>
                    <th>Web Site</th>
                    <th width="280px">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($empresas as $empresa)
                    <tr>
                        <td style="width: 75px;">
                            <image src="{{ @asset('storage/' . $empresa->logo) }}" class="img-thumbnail card-img" style="width: auto;"/>
                        </td>
                        <td>{{ $empresa->id }}</td>
                        <td>{{ $empresa->nome }}</td>
                        <td>{{ $empresa->email }}</td>
                        <td>{{ $empresa->website }}</td>
                        <td>
                            <form action="{{ route('admin.empresas.destroy',$empresa->id) }}" method="POST">
                                <a class="btn btn-primary" href="{{ route('admin.empresas.edit',$empresa->id) }}">Editar</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger" onclick="javascript: return confirm('Deseja realamente exluir?');">Excluir</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! $empresas->links() !!}
        </div>
    </div>
@endsection
