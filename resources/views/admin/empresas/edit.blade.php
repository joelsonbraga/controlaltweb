@extends('layouts.app')

@section('content')
    @include('admin.request-error')
    <div class="container">
        <div class="col-md-12">
            <div class="form-area">
                <form role="form" action="{{ route('admin.empresas.update', $empresa->getKey()) }}" method="POST" enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf
                    <br style="clear:both">
                    <h3 style="margin-bottom: 25px; text-align: center;">Editar Empresa</h3>
                    <div class="form-group">
                        <label for="nome">Name:</label><br>
                        <input type="text" class="form-control"  id="nome" name="nome" placeholder="nome" required value="{{$empresa->nome}}">
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail:</label><br>
                        <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="{{$empresa->email}}">
                    </div>
                    <div class="form-group">
                        <label for="url">Web site:</label><br>
                        <input type="url" class="form-control" id="website" name="website" placeholder="Web Site" value="{{$empresa->website}}">
                    </div>
                    <div class="form-group">
                        <label for="logo">Logo:</label><br>
                        <input type="file" class="form-control" name="logo" id="logo">
                    </div>
                    <div class="form-group" style="width: 100px">
                        <image src="{{ @asset('storage/' . $empresa->logo) }}" class="img-thumbnail card-img" style="width: auto;"/>
                    </div>
                    <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Enviar</button>
                    <button type="button" id="voltar" name="voltar" onclick="window.location.href='{{ route('admin.empresas.index') }}'" class="btn btn-info pull-left">Voltar</button>

                </form>
            </div>
        </div>
    </div>
@endsection
