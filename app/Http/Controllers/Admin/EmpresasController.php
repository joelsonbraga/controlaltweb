<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Empresas\Store;
use App\Http\Requests\Empresas\Update;
use App\Models\Empresa;
use App\Repositories\ImageRepository;
use App\Http\Controllers\Controller;

class EmpresasController extends Controller
{
    protected $imageRepository;

    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    public function index()
    {
        $empresas = Empresa::paginate(10);

        return view('admin.empresas.index', compact('empresas'));
    }

    public function create()
    {
        return view('admin.empresas.create');
    }

    public function store(Store $request)
    {
        try {
            $empresa = Empresa::create($request->only('nome', 'email', 'website'));
            $empresa->logo = $this->imageRepository->saveImage($request->file('logo'), $empresa->id);

            $empresa->save();
        } catch (\Exception | \Throwable $e) {
            return redirect()->route('admin.empresas.index')
                ->with('error', $e->getMessage());
        }

        return redirect()->route('admin.empresas.index')
            ->with('success', 'Empresa inserida com sucesso.');
    }

    public function edit(Empresa $empresa) {
        return view('admin.empresas.edit', compact('empresa'));
    }

    public function update(Update $request, Empresa $empresa) {
        try {

            $empresa->update($request->only('nome', 'email', 'website'));

            if ($request->file('logo')) {
                $this->imageRepository->deleteImage($empresa->logo, $empresa->id);
                $empresa->logo = $this->imageRepository->saveImage($request->file('logo'), $empresa->id);
            }

            $empresa->save();
        } catch (\Exception | \Throwable $e) {
            return redirect()->route('admin.empresas.index')
                ->with('error', $e->getMessage());
        }

        return redirect()->route('admin.empresas.index')
            ->with('success', 'Empresa atualizada com sucesso');
    }

    public function destroy(Empresa $empresa) {
        try {
            $empresa->delete();
            $this->imageRepository->deleteImage($empresa->logo, $empresa->id);
        } catch (\Exception | \Throwable $e) {
            return redirect()->route('admin.empresas.index')
                ->with('error', $e->getMessage());
        }

        return redirect()->route('admin.empresas.index')
            ->with('success', 'Empresa excluída com sucesso!');
    }
}
