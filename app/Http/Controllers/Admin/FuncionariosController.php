<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Funcionarios\Store;
use App\Http\Requests\Funcionarios\Update;
use App\Models\Empresa;
use App\Models\Funcionario;
use App\Http\Controllers\Controller;

class FuncionariosController extends Controller
{
    public function index()
    {
        $funcionarios = Funcionario::with('empresas')->paginate(10);

        return view('admin.funcionarios.index', compact('funcionarios'));
    }

    public function create()
    {
        $empresas = Empresa::all();
        return view('admin.funcionarios.create')->with('empresas', $empresas);
    }

    public function store(Store $request)
    {
        try {
            Funcionario::create($request->validated());

        } catch (\Exception | \Throwable $e) {
            return redirect()->route('admin.funcionarios.index')
                ->with('error', $e->getMessage());
        }

        return redirect()->route('admin.funcionarios.index')
            ->with('success', 'Funcionário inserido com sucesso.');
    }

    public function edit(Funcionario $funcionario)
    {
        $empresas = Empresa::all();
        return view('admin.funcionarios.edit', compact('funcionario'))->with('empresas', $empresas);
    }

    public function update(Update $request, Funcionario $funcionario) {
        try {

            $funcionario->update($request->validated());

        } catch (\Exception | \Throwable $e) {
            return redirect()->route('admin.funcionarios.index')
                ->with('error', $e->getMessage());
        }

        return redirect()->route('admin.funcionarios.index')
            ->with('success', 'Funcionário atualizado com sucesso');
    }

    public function destroy(Funcionario $funcionario) {
        try {
            $funcionario->delete();
        } catch (\Exception | \Throwable $e) {
            return redirect()->route('admin.funcionarios.index')
                ->with('error', $e->getMessage());
        }

        return redirect()->route('admin.funcionarios.index')
            ->with('success', 'Funcionário excluído com sucesso!');
    }
}
