<?php

namespace App\Http\Requests\Empresas;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'email' => 'required|email|unique:empresas,email',
            'website' => 'required|url',
            'logo' => 'required|image|mimes:jpeg,bmp,png,svg'
        ];
    }

    public function messages() {
        return [
            'nome.required'  => 'É necessário informar um nome para empresa!',
            'email.unique'   => 'Este e-mail já está sendo utilizado por outra empresa!',
            'email.required' => 'É necessário informar um e-mail para a empresa!',
            'website.required' => 'É necessário informar um web-site para a empresa!',
            'logo.required'    => 'É necessário selecionar uma logo para a empresa!',
        ];
    }

}
