<?php

namespace App\Http\Requests\Funcionarios;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'empresa_id' => 'required',
            'cpf' => 'required|unique:funcionarios,cpf',
            'nome' => 'required',
            'email' => 'required|email|unique:funcionarios,email',
            'telefone' => 'required',
        ];
    }

    public function messages() {
        return [
            'cpf.required'  => 'É necessário informar um CPF para o funcionário!',
            'cpf.unique'  => 'Este CPF já está sendo utilizado por outro funcionário!',
            'empresa_id.required' => 'É necessário informar uma empresa para o funcionário!',
            'nome.required'  => 'É necessário informar um nome para funcionário!',
            'email.unique'   => 'Este e-mail já está sendo utilizado por outro funcionário!',
            'email.required' => 'É necessário informar um e-mail para o funcionário!',
            'telefone.required' => 'É necessário informar um telefone para o funcionário!',
        ];
    }
}
