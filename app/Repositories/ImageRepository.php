<?php
namespace App\Repositories;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\UploadedFile;

class ImageRepository
{

    /**
     * @param UploadedFile $image
     * @param $id
     * @param string $type
     * @return \Exception|string
     */
    public function saveImage(UploadedFile $image, $id, $type = 'empresas'):string
    {
        try {
            $extension = $image->getClientOriginalExtension();
            $fileName = time() . random_int(100, 999) . '.' . $extension;

            $imageDir = 'images/' . $type . '/' . $id . '/';
            $destinationPath = storage_path('app/public/' . $imageDir);

            File::makeDirectory($destinationPath, 0755, true, true);

            $fullPath = $destinationPath . $fileName;

            Image::make($image)->save($fullPath);
        } catch (\Exception | \Throwable $e) {
            throw new $e;
        }

        return $imageDir . $fileName;

    }

    /**
     * @param string $file
     * @return bool
     */
    public function deleteImage(string $file, $id = null):bool
    {
        try {

            $destinationPath = storage_path('app/public/');

            File::delete($destinationPath . $file);

        } catch (\Exception | \Throwable $e) {
            throw new $e;
        }

        return true;
    }
}
