<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
    protected $table = 'funcionarios';
    protected $primaryKey = 'cpf';
    protected $keyType = 'string';
    protected $fillable = [
        'cpf',
        'empresa_id',
        'nome',
        'telefone',
        'email'
    ];
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    protected $hidden = [
        'deleted_at'
    ];


    public function empresas()
    {
        return $this->belongsTo(Empresa::class, 'empresa_id');
    }
}
