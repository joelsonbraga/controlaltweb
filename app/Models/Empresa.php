<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresas';
    protected $fillable = [
        'nome',
        'email',
        'website',
        'logo'
    ];

    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
    ];
    protected $hidden = [
      'deleted_at'
    ];

    public function funcionarios()
    {
        return $this->hasMany(Funcionario::class, 'empresa_id');
    }
}
