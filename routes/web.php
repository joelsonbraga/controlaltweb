<?php

Route::get('/', function () {
    return view('auth.login');
});

Route::prefix('admin')->name('admin.')->namespace('Admin')->middleware(['auth'])->group(function() {
    Route::resource('empresas', 'EmpresasController');
    Route::resource('funcionarios', 'FuncionariosController');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

